delimiter //
DROP PROCEDURE IF EXISTS COUNTRIES_HOUR; //

CREATE PROCEDURE COUNTRIES_HOUR()
BEGIN
	DECLARE x INT DEFAULT 0;
	SET x = 0;
    
	WHILE x < 24 DO
		DROP TABLE IF EXISTS TEMP_COUNTRIES_HOUR;

		CREATE TEMPORARY TABLE IF NOT EXISTS TEMP_COUNTRIES_HOUR AS (
			SELECT 
				nname,
				idPlayer,
				country, 
				SUM(((TIME_TO_SEC(exitTime) - TIME_TO_SEC(joinedTime)) / 60)) AS totalSessionTime,
				COUNT(1) AS connects
			FROM LogPlayerSession
			INNER JOIN Player ON LogPlayerSession.idPlayer = Player.id
			WHERE HOUR(joinedTime) = x
			AND country != 'NULL'
			AND nname != 'NULL'
			GROUP BY idPlayer
		);

		SELECT 
			UPPER(country) AS c, -- country
			COUNT(1) AS cc, -- countryConnects
			ROUND(SUM(totalSessionTime)) AS ctst, -- countryTotalSessionTime
			ROUND(AVG(totalSessionTime)) AS cast -- countryAverageSessionTime
		FROM TEMP_COUNTRIES_HOUR
		GROUP BY c
		HAVING ctst > 0;
        
        SET x = x + 1;
	END WHILE;
END//